#====================================================#
#     N-th test of Surface Brightness Calculation    #
#====================================================#
print('\nN-th TRIAL: SURFACE BRIGHTNESS WITH TNG\n')

import numpy as np
import h5py
import time
import sys
import illustris_python as il
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

suite = 'IllustrisTNG'
simulation = 'L75n1820TNG'

basePath = '/virgo/simulations/%s/%s/output' % (suite, simulation) # /postprocessing/sizes_projected (en lugar de output)
amdir = '/u/vrg/AngularMomentum/output/%s/%s' % (suite,simulation)

snapnum = 99			# Corresponding to z = 0
z = 0.0485236299818		# Set ad hoc redshift for SDSS (observationally stuff)

import astropy.constants as cte
import astropy.units as u
from astropy.cosmology import Planck15		# Planck Collaboration 2015

cosmo = Planck15
#h = ((cosmo.H(0)).value)/100		# Adimentional Hubble's constant
#box_size = (75e3 /h) / (1+z)		# Box size

# Load some info from snapshot header (including h)
f = h5py.File('%s/snapdir_%s/snap_%s.0.hdf5' % (basePath, str(snapnum).zfill(3), str(snapnum).zfill(3)), 'r')
header = dict(f['Header'].attrs.items())
h = header['HubbleParam']
box_size = header['BoxSize']
redshift = header['Redshift']

box_size = (box_size/h) / (1+redshift)	


""" SOME USEFUL FUNCTIONS """
def transform(x, jvec, proj_kind='xy'):
    """
    Return a projection of the particle positions. In all cases we only
    return the first two coordinates (x, y).

    Parameters
    ----------
    x : array-like
        2-dimensional array (Nx3) with the particle positions.
    jvec : array-like
        Direction of the galaxy's total stellar angular momentum.
    proj_kind : str, optional
        Specify which kind of projection. The possible values are:

        'yz' : Project onto the yz plane.
        'zx' : Project onto the zx plane.
        'xy' : Project onto the xy plane.
        'planar' : Rotate around z axis until jvec is in the yz plane. Return xy.
        'edgeon' : jvec perpendicular to z-axis.
        'faceon' : jvec parallel to z-axis.
        
    Returns
    -------
    x_new : array-like
        2-dimensional array (Nx2) with the projected particle positions.

    """
    assert len(jvec) == x.shape[1] == 3

    # Define unit basis vectors of new reference frame
    if proj_kind == 'yz':
        e1 = np.array([0,1,0])
        e2 = np.array([0,0,1])
    elif proj_kind == 'zx':
        e1 = np.array([0,0,1])
        e2 = np.array([1,0,0])
    elif proj_kind == 'xy':
        e1 = np.array([1,0,0])
        e2 = np.array([0,1,0])
    elif proj_kind == 'planar':
        # New y-axis is the projection of jvec onto the xy plane
        e2 = np.array([jvec[0], jvec[1], 0.0])
        e2 = e2 / np.linalg.norm(e2)  # normalize
        e1 = np.cross(e2, np.array([0,0,1]))
    elif proj_kind == 'edgeon':
        # New y-axis is aligned with jvec
        e2 = jvec[:] / np.linalg.norm(jvec)  # normalize
        e1 = np.cross(e2, np.array([0,0,1]))
    elif proj_kind == 'faceon':
        # New z-axis is aligned with jvec
        e3 = jvec[:] / np.linalg.norm(jvec)  # normalize
        # New x-axis is chosen to coincide with edge-on projection
        e1 = np.cross(e3, np.array([0,0,1]))
        e2 = np.cross(e3, e1)
    else:
        raise Exception('Projection kind not understood.')

    # Project onto new axes
    x_new = np.zeros((x.shape[0], 2), dtype=np.float64)
    x_new[:,0] = np.dot(x, e1)
    x_new[:,1] = np.dot(x, e2)
    
    return x_new
    
def Remove_periodic_boundaries(x):
	"""
	Removes any periodic boundary conditions (including the first particle).
	
    Parameters
    ----------
    x : array-like
        2-dimensional array (Nx3) with the particle positions.
        
    Returns
    -------
    dx : array-like
        2-dimensional array (Nx3) with the corrected particle positions.
        
	"""
	dx = x[:] - x[0]
	dx = dx - (np.abs(dx) > 0.5*box_size) * np.copysign(box_size,dx - 0.5*box_size)
	
	return dx

def Find_R(x,fraction):
	"""
	Returns a proxy of the radius containing a given fraction of the total stellar mass,
	using the cumulative distribution and a linear interpolation.
	
	Parameters
	----------
	x : array-like
		2-dimensional array (Nx2) with the projected particle positions.
	fraction : scalar-like
		Fraction of the total stellar mass used to delimit athe radius of interest, 
		e.g. for Rhalf (containing half of the total stellar mass), set fraction = 0.5.
		
	Returns
	-------
	R_eff =  scalar-like
		Radius containing the given fraction of the total stellar mass, in kpc.
	
	"""
	r = np.sqrt(x[:,0]**2 + x[:,1]**2)		# Radial distance of the disc particles
	
	indexes = np.argsort(r)		# Sorted indexes of r
	r_sort = r[indexes]
	M_sort = mass[indexes]
	
	# Calculate the cumulative function
	M_cum = np.cumsum(M_sort)
	M_cum = M_cum / np.sum(M_sort)
	
	# Linear interpolation over the cumulative function
	index = np.searchsorted(M_cum,fraction)
	m = (M_cum[index-1]-M_cum[index]) / (r_sort[index-1]-r_sort[index])
	
	R_eff = r_sort[index] - (1./m)*(M_cum[index]-fraction)
	
	return r, R_eff
	
def Find_phot_rad(x,photometry,fraction):
	"""
	Returns a proxy of the radius containing a given fraction of the total stellar luminosity 
	(or flux), measured in a specific band, e.g. r-band of SDSS, using the cumulative
	distribution and a linear interpolation.
	
	Parameters
	----------
	x : array-like
		2-dimensional array (Nx2) with the projected particle positions.
	photometry : array-like
		1-dimensional array (Nx1) with the stellar photometrics of the particles.
	fraction: scalar-like
		Fraction of the total stellar luminosity used to delimit the radius of interest,
		e.g. for Rexp (radius in which the total luminosity (flux) drops 1/e times), set 
		fraction = 1-1/e.
	
	Returns
	-------
	R_phot : scalar-like
		Radius containing the given fraction of the total luminosity (flux), in kpc.
	
	"""
	r = np.sqrt(x[:,0]**2 + x[:,1]**2)		# Radial distance of the disc particles
	
	# Convert to fluxes (in units of Flux0, the flux that corresponds to a
	# zero magnitude, or maggies in SDSS nomenclature):
	Flux = 10.0**(-2.0/5.0*photometry)
	
	indexes = np.argsort(r)		# Sorted indexes of r
	r_sort = r[indexes]
	Flux_sort = Flux[indexes]
	
	# Calculate the cumulative function
	Flux_cum = np.cumsum(Flux_sort)
	Flux_cum = Flux_cum / np.sum(Flux_sort)

	# Linear interpolation over the cumulative function
	index = np.searchsorted(Flux_cum,fraction)
	m = (Flux_cum[index-1]-Flux_cum[index]) / (r_sort[index-1]-r_sort[index])
	
	R_phot = r_sort[index] - (1./m)*(Flux_cum[index]-fraction)
	
	return R_phot
	
def Get_magnitude(r, photometrics, radius):
	"""
	Returns the apparent magnitude in a given band, considering the stellar particles
	within a given radius.
	
	Parameters
	----------
	r : array-like
		1-dimentional array (Nx1) with the distance of the particles relative to
		the center of the galaxy.
	photometrics : array-like
		1-dimentional array (Nx1) with the stellar photometrics of the particles.
	radius : scalar-like
		Cut-off radius in which the magnitudes are measured.
	
	Returns
	-------
	m_eff : scalar-like
		The "effective" apparent magnitude of the galaxy.
		
	"""
	locs_eff = (r < radius)
	c = ((cte.c).to('km/s')).value	# Speed of light in km/s
	
	# TO DO: use luminosity distance instead of Hubble approximation
	
	#d_Mpc = (c*z / (100*h)) * u.Mpc		# Distance to the galaxy, obtained from Hubble's law
	d_Mpc = cosmo.luminosity_distance(z)	# Equal to 222.61 Mpc 
	d_pc = d_Mpc.to('pc')
	
	# Convert absolute magnitudes to fluxes
	m_ap = photometrics[locs_eff] + 5*np.log10(d_pc.value) - 5	
	Flux = 10**(-0.4 * m_ap)
	
	# Compute the total flux of the stellar particles and get the "effective" magnitude
	Flux_eff = np.sum(Flux)
	m_eff = -2.5*np.log10(Flux_eff)
	
	return m_eff

def Get_Surface_Brightness(m_x,scale_length):
	"""
	Returns the surface brightness in different SDSS bands
	
	Parameters
	----------
	m_x : array-like
		1-dimentional array (Nx1) with the apparent x-band magnitudes of the stellar components
	scale_lenght : scalar-like
		Scale lenght used to compute the surface brightness (alpha in eq. (2) of Pérez-Montaño 
		& Cervantes Sodi (2019))
	
	Returns
	-------
	mu_x : scalar-like
		Central surface brightness in the x-band
		
	"""
	# Convert kpc to arcsec
	scale = cosmo.kpc_proper_per_arcmin(z)
	scale_arcsec = (scale).to('kpc/arcsec')
	
	scale_length_arcsec = scale_length / scale_arcsec.value
	Area_eff = np.pi * (scale_length_arcsec**2) 
	
	# x-band average surface brightness within Reff
	mu_x = m_x + 2.5*np.log10(Area_eff)
	
	return mu_x
	
def Get_Stellar_Surface_Density(x,scale_length):
	"""
	Returns the stellar mass urface density within a given radius (or scale-length)
	
	Parameters
	----------
	x : array-like
		2-dimensional array (Nx2) with the projected particle positions.
	scale_lenght : scalar-like
		Radius used to compute the stellar mass surface brightnes as M(r < scale-length)/Area
		
	Returns
	-------
	stel_dens : scalar-like
		Stellar mass surface density within the scale length, in uits of M_sun / pc^2
	"""
	# Calculate surface density
	r = np.sqrt(x[:,0]**2 + x[:,1]**2)	# Radial distance of the disc particles
	
	locs_in = (r < scale_length)
	
	indexes = np.argsort(r)		# Sorted indexes of r
	r_sort = r[indexes]
	M_sort = mass[indexes]
	
	M_in = M_sort[locs_in]
	
	stel_dens = np.sum(M_in)/(np.pi*(scale_length * 1000)**2)	
	
	return stel_dens


# Find out total number of subhalos
header = il.groupcat.loadHeader(basePath, snapnum)
Nsubs = header['Nsubgroups_Total']

print('Total number of subhalos in %s: %d' % (simulation, Nsubs))

N_LSB = 0
N_HSB = 0

t0 = time.time()

mass = il.groupcat.loadSubhalos(basePath, snapnum, fields=['SubhaloMassType'])
stel_mass = mass[:,4]
mstar = stel_mass * 1e10 / h

# Identifying central galaxies
group_first_sub = il.groupcat.loadHalos(basePath, snapnum, fields=['GroupFirstSub'])
sub_gr_nr = il.groupcat.loadSubhalos(basePath, snapnum, fields=['SubhaloGrNr'])

gal_type = 'satellites'

is_central = group_first_sub[sub_gr_nr] == np.arange(Nsubs, dtype=np.uint32)
ID_central = np.where(is_central == True)
ID_satelite = np.where(is_central == False)

if gal_type == 'centrals':
	IDs = ID_central[0]
elif gal_type == 'satellites':
	IDs = ID_satelite[0]
else:
	IDs = [252245]
	

print('Number of %s in %s: %d' % (gal_type, simulation, len(IDs)))


# Proceed with the output 
band = 'r'
mu_0 = 22.0

filename = '/u/lepm09/LSB/%s_SurfaceBrightness_%s_masscutTNG100_%s-band_R50_%.1f.txt' % (simulation,gal_type,band,mu_0)


print('\nWorking on %s-band samples, just wait...' % (band))

mass_limit = 10**(8.5)

with open(filename,'w') as file_output:
	for sub_id in IDs:
		if mstar[sub_id] > mass_limit:
		#if mstar[sub_id] > 10**(8+(bin*0.5)) and mstar[sub_id] < 10**(8+(bin+1)*0.5):
			# Loading data for a given subhalo
			stel_mass = il.snapshot.loadSubhalo(basePath, snapnum, sub_id, 'stars', fields=['Masses'])
			stel_coord = il.snapshot.loadSubhalo(basePath, snapnum, sub_id, 'stars', fields=['Coordinates'])
			stel_phot = il.snapshot.loadSubhalo(basePath, snapnum, sub_id, 'stars', fields=['GFM_StellarPhotometrics'])
			
			formtimes = il.snapshot.loadSubhalo(basePath, snapnum, sub_id, 'stars', fields=['GFM_StellarFormationTime'])
			
			# Remove wind particles
			locs_notwind = formtimes >= 0
			coords = (stel_coord[locs_notwind]/h) / (1+redshift)	# In units of kpc
			mass = stel_mass[locs_notwind] * 1e10 / h				# In M_sun
			
			# Remove periodic boundaries
			x = Remove_periodic_boundaries(coords)
			
			# Transform particle positions according to 'proj_kind' (2D projection)
			with h5py.File('%s/jstar_%03d.hdf5' % (amdir, snapnum), 'r') as f:
				jstar_direction = f['jstar_direction'][:]
				
			x_new = transform(x, jstar_direction[sub_id], proj_kind='faceon')
			
			# Calculate effective radius of the galaxy (containing 50% of the mass)
			r, R_half = Find_R(x_new,0.5)
			#print ('\nEffective radius at R = %f kpc' % (R_half))
			
			# Get the absolute magnitude in the SDSS g,r-bands
			M_g = stel_phot[:,4]	# SDSS g-band stellar magnitude
			M_r = stel_phot[:,5]	# SDSS r-band stellar magnitude 
			
			# Get absolute magnitude in the Johnson's B-band
			M_B = M_g[locs_notwind] + 0.47*(M_g[locs_notwind] - M_r[locs_notwind]) + 0.17
			
			# Calculate the exponential radius (at which luminosity drops 1/e times the total)
			R_exp_r = Find_phot_rad(x_new,M_r[locs_notwind],1-(1/np.e))
			R_exp_g = Find_phot_rad(x_new,M_g[locs_notwind],1-(1/np.e))
			#print ('Exponential radius at R = %f kpc' % (R_exp))
			
			# Calculate R_50, the radius containing 50% of the total luminosity
			R_50_r = Find_phot_rad(x_new,M_r[locs_notwind],0.5)
			R_50_g = Find_phot_rad(x_new,M_g[locs_notwind],0.5)
			
			R_50_B = Find_phot_rad(x_new,M_B,0.5)
			
			# Calculate R_90, the radius containing 90% of the total luminosity
			R_90_r = Find_phot_rad(x_new,M_r[locs_notwind],0.9)
			R_90_g = Find_phot_rad(x_new,M_g[locs_notwind],0.9)
			#print ('Petrosian radius at R = %f kpc' % (R_pet))
			
			# Compute the apparent magnitude of the stellar particles within an effective radius
			m_g_eff = Get_magnitude(r,M_g[locs_notwind],R_50_g)
			m_r_eff = Get_magnitude(r,M_r[locs_notwind],R_50_r)
			
			# Calculate the Stellar Mass Density 
			stel_dens = Get_Stellar_Surface_Density(x_new,R_half)
			
			# Obtaining surface brightness
			mu_g = Get_Surface_Brightness(m_g_eff,R_50_g)
			mu_r = Get_Surface_Brightness(m_r_eff,R_50_r)
			
			mu_B = mu_g + 0.47*(mu_g - mu_r) + 0.17 	# B-band central surface brightness
			
			#print('B-band central surface brightness = %f mag/arcsec^2' % (mu_B)) 
			if mu_r > mu_0:
				N_LSB = N_LSB + 1
			
			elif mu_r < mu_0 and mu_B > 0:
				N_HSB = N_HSB +1
				
			else:
				pass
			
			file_output.write('%d\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n' % (sub_id, R_half, R_exp_r, R_50_r, R_90_r, mu_g, mu_r, mu_B,stel_dens))
			#print('\nFinished for subhalo %d' % (sub_id))
		
		else: 
			pass


#=============================================
print('\nTotal of LSBs: %d\nTotal of HSBs: %d ' % (N_LSB, N_HSB)) 
print('\nTotal execution time: %f min' % ((time.time() - t0)/60))
print('\nFinished for %s galaxies' % (gal_type))
print('\nDONE!\n')		
